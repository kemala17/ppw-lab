//Chat box
function appendChat(e) {
    var id = e.target.id;
    var notEmpty = $(".usertext").val() != "",
        isEnterKeypress = e.type == "keypress" && e.keyCode == 13,
        isSendClick = e.type == "click" && id == "send";

    if( notEmpty && (isEnterKeypress || isSendClick) ) {
		var txt = "<b>me: </b>"+$(".usertext").val();
		var rply = ["<b>admin: </b> Need help?",
					"<b>admin: </b> Yoi",
					"<b>admin: </b> Kenapa Bisa gitu",
					"<b>admin: </b> Gue sih ga ya",
					"<b>admin: </b> APASIH LU GAJE",
					"<b>admin: </b> Capek bet sumpiww",
					"<b>admin: </b> Lo harus usaha lebih sih",
					"<b>admin: </b> mangat ma bro",
					]
		var min = 0;
		var max = 7;
		var random = Math.floor(Math.random() * (max - min + 1)) + min;
		$('<p>'+txt+'</p>').addClass('msg-send').appendTo('.msg-insert');
		$(".usertext").val("");
		$('<p>'+rply[random]+'</p>').addClass('msg-receive').appendTo('.msg-insert');
		$(".chat-body").scrollTop($(".chat-body").height());
    }
}
$(".chav_box_in").keypress(appendChat);

function minimChatBox() {
  	var element = document.getElementById("arrow");
    if (element.className === "normal") {
    	$(".chat-body").hide();
		element.className = "rotate";
	}
	else if ( element.className === "rotate") {
		$(".chat-body").show();
		element.className = 'normal';
	}
}
//end Chat Box

// Calculator
var print = document.getElementById('print');
var displayValue = '0';  
var erase = false;

var go = function(x) {
	if (x === 'ac') {
		print.value = '';
	}
	else if (x === 'log') {
		print.value += 'log(';
	}
	else if (x === 'tan') {
		print.value +='tan(';
	}
	else if (x === 'sin') {
		print.value +='sin(';
	}
	else if (x === 'eval') {
		print.value = Math.round(evil(print.value) * 10000) / 10000;
		erase = true;
	}
	else {
		print.value += x;
	}
	$('#print').val(print.value).html();

};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END Calculator


//Select Theme
var themes =[{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
   {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
   {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
   {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
   {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}]

var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
localStorage.setItem("themes", JSON.stringify(themes));

var myselect = $('.my-select').select2();

myselect.select2({
'data': JSON.parse(localStorage.getItem("themes"))
})

var themeArray = JSON.parse(localStorage.getItem("themes"));
var selectedTheme =themeArray[3];
var lastTheme = selectedTheme;

if(localStorage.getItem("lastTheme") !== null){
lastTheme = JSON.parse(localStorage.getItem("lastTheme"));
}

selectedTheme = lastTheme;
$('body').css({"background-color":selectedTheme.bcgColor, "font-color":selectedTheme.fontColor});

$('.apply-button').on('click',function(){
var themeName = myselect.val();
selectedTheme = themeArray[themeName];
$('body').css({'backgroundColor': selectedTheme.bcgColor, 'font-color':selectedTheme.fontColor});

localStorage.setItem("lastTheme",JSON.stringify(selectedTheme));
});
//END Select Theme
