from django.conf.urls import url
from .views import *

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add-friend/$', add_friend, name='add-friend'),
	url(r'^validate-npm/$', validate_npm, name='validate-npm'),
	url(r'^delete-friend/$', delete_friend, name='delete-friend'),
	url(r'^get-friend-list/$', friend_list, name='get-friend-list'),
	url(r'^friends/$', getFriendList, name='all'),
	url(r'display-friend/(?P<pk>[0-9]+)/$', displayBio, name='display-friend'),
]
