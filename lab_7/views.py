from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.db.models.fields.related import ManyToManyField
from .models import Friend
from Packages.api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper(os.environ.get("SSO_USERNAME"),
                         os.environ.get("SSO_PASSWORD"))

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada
	auth = csui_helper.instance.get_auth_param_dict()
	mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
	friend_list = Friend.objects.all()
	response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list, "access_token": auth['access_token'], "client_id": auth["client_id"]}
	html = 'lab_7/lab_7.html'
	return render(request, html, response)

def friend_list(request):
	friend_list = Friend.objects.all()
	response['friend_list'] = friend_list
	response['access_token'] = csui_helper.instance.get_auth_param_dict()['access_token']
	response['client_id'] = csui_helper.instance.get_auth_param_dict()['client_id']
	html = 'lab_7/daftar_teman.html'
	return render(request, html, response)

def getFriendList(request):
	list = Friend.objects.all()
	hasil = []
	for x in list:
		hasil.append(to_dict(x))
	return JsonResponse({'daftar':hasil})

@csrf_exempt
def add_friend(request):
	if request.method == 'POST':
		name = request.POST['name']
		npm = request.POST['npm']
		alamat = request.POST.get('alamat')
		ttl = request.POST.get('ttl')
		prodi = request.POST.get('prodi')
		isValid = npmValid(npm)
		data = {'valid':isValid}
		if isValid:
			friend = Friend(friend_name=name, npm=npm, alamat=alamat, ttl=ttl, prodi=prodi)
			friend.save()
			data['teman'] = to_dict(friend)
		return JsonResponse(data)

@csrf_exempt
def delete_friend(request):
    if request.method == 'POST':
        friend_id = request.POST['friend_id']
        Friend.objects.filter(id=friend_id).delete()
        selector = '#'+friend_id
        data = {'selector':selector}
        return JsonResponse(data)

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    isAda = not npmValid(npm);
    data = {
        'is_taken': isAda
    }
    return JsonResponse(data)

def to_dict(instance):
	opts = instance._meta
	data = {}
	for f in opts.concrete_fields + opts.many_to_many:
		data[f.name] = f.value_from_object(instance)
	return data


def npmValid(npm):
	allFriend = Friend.objects.all()
	for x in allFriend :
		if x.npm == npm:
			return False
	return True

def displayBio(request, pk):
    html = 'lab_7/biodata.html'
    response['teman'] = Friend.objects.filter(id=pk)[0]
    return render(request, html, response)
