from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.
response = {}
def index(request):    
    html = 'lab_6/lab_6.html'
    response['author'] = 'Kemala Andiyani'
    return render(request, html, response)